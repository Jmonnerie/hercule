/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/02/14 18:12:54 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/02/16 19:12:39 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "hydra.h"

int		main(void)
{
	int					sockfd;
	char				sendline[256];
	char				recvline[256];
	struct sockaddr_in	servaddr;

	if (!(sockfd = socket(AF_INET,SOCK_STREAM,0)))
		return (-1);
	ft_bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(2904);
	servaddr.sin_addr.s_addr = INADDR_ANY;
	if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0)
		return (-1);
	while (1)
	{
		ft_bzero(sendline, 256);
		ft_bzero(recvline, 256);
		if (read(0, sendline, 256) == -1)
			return (-1);
		send(sockfd, sendline, 256, 0);
		recv(sockfd, recvline, 256, 0);
		sleep(1);
		ft_putstr(recvline);
	}
	return (0);
}

/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   server.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/02/14 19:11:24 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/02/16 19:12:41 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "hydra.h"

static int		ft_error(int i, char *str)
{
	dprintf(1, "%s\n", str);
	if (i == 1)
		write(2, "Error while processing\n", 23);
	else
		write(2, "This server don't need an argument !\n", 37);
	return (-1);
}

char		*ft_serv(int listen_fd, int comm_fd)
{
	char				str[10];
	struct sockaddr_in	servaddr;

	if (!(listen_fd = socket(PF_INET, SOCK_STREAM, 0)))
		return ("1");
	ft_bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = PF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(2904);
	if (bind(listen_fd, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0 ||
			listen(listen_fd, 10) != 0)
		return ("3");
	if (!(comm_fd = accept(listen_fd, NULL, NULL)))
		return ("4");
	while (1)
	{
		ft_bzero(str, 10);
		read(comm_fd, str, 4);
		if (ft_strncmp(str, "ping", 4) == 0)
			write(comm_fd, "pong pong\n", 10);
		else
			write(comm_fd, "\0", 1);
	}
	return (NULL);
}

int			main(int argc, char **argv)
{
	char				*str;

	str = NULL;
	(void)argv;
	if (argc == 1 && !(str = ft_serv(0, 0)))
		return (ft_error(1, str));
	else
		return (ft_error(2, str));
	return (1);
}

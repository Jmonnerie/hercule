file=/etc/ssh/sshd_config

if [ $(id -u) != "0" ];then
	echo "You need root access rights!"
	exit 1
fi

port=$(cat $file | grep "Port ")

apt-get update
apt-get upgrade -y
apt-get install openssh-client -y
apt-get install openssh-server -y
apt-get install vim -y

if [ $1 ];then
	vim $file -c ":%s/$port/Port $1/g" -c :x
else
	vim $file -c ":%s/$port/Port 1000/g" -c :x
fi

service ssh restart
echo "Done :D"
